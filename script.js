function constructImageURL (photoObj) {
    return "https://farm" + photoObj.farm +
            ".staticflickr.com/" + photoObj.server +
            "/" + photoObj.id + "_" + photoObj.secret + ".jpg";
}
const displayImages = (link,destinohtml)=>{
    let destino = document.getElementById(destinohtml);
    let figure = document.createElement('figure');
    let image = document.createElement('img');
    image.src = link;
    image.className = "box";
    figure.appendChild(image);
    destino.appendChild(figure);
}

const API_Access = async ()=>{
    let retorno={};
    const resfotos = await fetch("https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?api_key=5a495c7b7f7527ffe1f26c5833b1d93a&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=250&text=Border-Collie");
    const json = await resfotos.json();   
    const photos_array = await json.photos.photo;
    console.log(photos_array);
    for(let i in photos_array){
        retorno[i]= await constructImageURL(photos_array[i]);
    }
    return retorno;
}
API_Access().then(res=>{
    for(let i in res){
        displayImages(res[i],"galery");
    }
});
